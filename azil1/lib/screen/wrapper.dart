import 'package:azil1/screen/authenticate1/authenticatee.dart';
import 'package:azil1/screen/home/home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:azil1/models/user.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    final user = Provider.of<User>(context);
    print(user);

    //return either home or authenticate widget
    if(user==null){
      return Authenticate();
    }else{
      return Home();
    }
  }
}
