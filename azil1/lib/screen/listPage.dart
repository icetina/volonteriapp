
import 'dart:async';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:azil1/timer/timer1.dart';

class ListPage extends StatefulWidget {
  @override
  _ListPageState createState() => _ListPageState();
}
class _ListPageState extends State<ListPage> {
  
  

  Future getDogs()async{
    var firestore=Firestore.instance;
    QuerySnapshot qn =await firestore.collection("dogs").getDocuments();
    return qn.documents;
  }
  
  Future<Null>getRefresh()async{
    await Future.delayed(Duration(seconds: 3));
    setState(() {
      getDogs();
    });
  }

  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      body: FutureBuilder(
        future: getDogs(),
        builder: (context,snapshot){
          if(snapshot.connectionState==ConnectionState.waiting){
            return Center(
              child: CircularProgressIndicator(),
            );


          }else{
            return RefreshIndicator(
              onRefresh: getRefresh,
              child: ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (context, index){

                var ourData=snapshot.data[index];

                return Container(
                  height: 550,
                  margin: EdgeInsets.all(10.0),
                  child: Card(
                    elevation: 10.0,

                    child: Column( 
                      children: <Widget>[
                        //first
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              
                              //Circle avatar
                              Container(
                                child: Row(
                                  children: <Widget>[

                                    Container(
                                      margin: EdgeInsets.all(10.0),
                                      child: CircleAvatar(
                                        child:Text(ourData.data['ime'][0], 
                                        style: TextStyle(fontSize: 20.0),),
                                        backgroundColor: Colors.blueAccent,
                                ),
                              ),
                              //naslov
                                    SizedBox(width: 5.0,),
                                     Container(
                                          child: Text(ourData.data['ime'].toString().toUpperCase(),
                                          style: TextStyle(
                                            fontSize: 20.0, 
                                            color: Colors.blueAccent,
                                            fontWeight: FontWeight.bold),
                                          ),
                                      ),
                                    ],
                                    ),
                                    ),


                              
                          ],
                          )
                          ),

                          SizedBox(height: 10.0,),

                          Container(
                            margin: EdgeInsets.all(10.0),
                            child:ClipRRect(
                              borderRadius:BorderRadius.circular(15.0),
                              child: Image.network(ourData.data['slika'],
                              height: 250.0,
                              width: MediaQuery.of(context).size.width,
                                fit: BoxFit.cover,
                                ),
                              ),
                            ),

                          SizedBox (height: 8.0,),
                          Container(
                            margin: EdgeInsets.all(10.0),
                            child: Text(ourData.data['pasmina'],
                            style: TextStyle(
                              fontSize: 17.0, 
                              color: Colors.black
                            ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.all(10.0),
                            child: Text(ourData.data['godiste'],
                            style: TextStyle(
                              fontSize: 17.0, 
                              color: Colors.black
                            ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.all(10.0),
                            child: Text(ourData.data['tezina'],
                            style: TextStyle(
                              fontSize: 17.0, 
                              color: Colors.black
                            ),
                            ),
                          ),

                          RaisedButton(
                              color: Colors.grey[300],
                              child: Text(
                                'Odaberi psa',
                                style: TextStyle(color: Colors.indigo),
                              ),
                              onPressed: () async{
                                Navigator.push(context, MaterialPageRoute(builder: (context){
                                  return Timer();
                                  }
                                ));})
          
                      ],
                    ),
                  ),
                );
                
              
            }));
        }
      },),
    );
  }

customDialog(BuildContext context, String slika, String ime, String godiste){
  return showDialog(
    context: context, 
    builder: (BuildContext context){
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0)
      ),
      child: Container(
        height: MediaQuery.of(context).size.height/1.20,
        width: MediaQuery.of(context).size.width,

      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20.0),
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [
              Colors.deepOrange,
              Colors.deepOrangeAccent,
              Colors.greenAccent
            ]
            )
            ),

            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[

                  Container(
                  height: 150.0,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20.0),
                    child: Image.network(slika, height: 150.0, width: MediaQuery.of(context).size.width, fit: BoxFit.cover,),),
                  ),

                  SizedBox(height: 6.0,),
                  Container(
                    padding: EdgeInsets.all(10.0),
                    child: Text(ime.toUpperCase(),
                    style:TextStyle(
                      fontSize: 20.0, 
                      color: Colors.white),),),

                  SizedBox(height: 6.0,),
                  Container(
                    padding: EdgeInsets.all(10.0),
                    child: Text(godiste.toUpperCase(),
                      style:TextStyle(
                        fontSize: 20.0, 
                        color: Colors.white),),),



              ],),),

      ), 
      );
  }
  );
}
}
