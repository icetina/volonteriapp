import 'package:azil1/screen/listPage.dart';
import 'package:azil1/services/auth.dart';
import 'package:flutter/material.dart';

class Home extends StatelessWidget {

  final AuthService _auth=AuthService();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text('DOBRODOŠLI'),
          backgroundColor: Colors.indigo,
          elevation: 0.0, 
          actions: <Widget>[
            FlatButton.icon(
              icon: Icon(Icons.person), 
              label: Text('logout'),
              onPressed: ()async{
                await _auth.signOut(); 
              },
            ),
          ],
        ),
        body: ListPage(),
      ),
    );
  }
}
